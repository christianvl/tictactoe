# Tic Tac Toe

<p>This is a simple C program to play tic tac toe in the console.

## License
<p> This software is licensed under the [AGPL3](https://www.gnu.org/licenses/agpl-3.0.html) license.
<p> ![AGPL](https://www.gnu.org/graphics/agplv3-88x31.png "AGPL3")
