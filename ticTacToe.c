/*
 * Christian van Langendonck
 * 2020-10
 * Tic Tac Toe game
*/
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

bool haveWinner();
void drawBoard();
void markBoard();
bool pMove(int selection);
int p1Move = 0, p2Move = 0;
char moves[10] = {'0','1','2','3','4','5','6','7','8','9'};

int main() {
  while (!haveWinner()) {
    markBoard();
  }
  return 0;
}

bool haveWinner() {
  bool toReturn = false;
  if (moves[1] == 'X') {
    if (moves[2] == 'X' && moves[3] == 'X') toReturn = true;
    else if (moves[4] == 'X' && moves[7] == 'X') toReturn = true;
    else if (moves[5] == 'X' && moves[9] == 'X') toReturn = true;
  }
  else if (moves[2] == 'X') {
    if (moves[5] == 'X' && moves[8] == 'X') toReturn = true;
  }
  else if (moves[3] == 'X') {
    if (moves[6] == 'X' && moves[9] == 'X') toReturn = true;
    else if (moves[5] == 'X' && moves[7] == 'X') toReturn = true;
  }
  else if (moves[5] == 'X') {
    if (moves[4] == 'X' && moves[6] == 'X') toReturn = true;
  }
  else if (moves[8] == 'X') {
    if (moves[7] == 'X' && moves[9] == 'X') toReturn = true;
  }


  else if (moves[1] == 'O') {
    if (moves[2] == 'O' && moves[3] == 'O') toReturn = true;
    else if (moves[4] == 'O' && moves[7] == 'O') toReturn = true;
    else if (moves[5] == 'O' && moves[9] == 'O') toReturn = true;
  }
  else if (moves[2] == 'O') {
    if (moves[5] == 'O' && moves[8] == 'O') toReturn = true;
  }
  else if (moves[3] == 'O') {
    if (moves[6] == 'O' && moves[9] == 'O') toReturn = true;
    else if (moves[5] == 'O' && moves[7] == 'O') toReturn = true;
  }
  else if (moves[5] == 'O') {
    if (moves[4] == 'O' && moves[6] == 'O') toReturn = true;
  }
  else if (moves[8] == 'O') {
    if (moves[7] == 'O' && moves[9] == 'O') toReturn = true;
  }

  return toReturn;
}

void drawBoard(char moves[]) {
  printf("\n\t%c\t|\t%c\t|\t%c\n", moves[1], moves[2], moves[3]);
  printf("----------------|---------------|--------------\n");
  printf("\t%c\t|\t%c\t|\t%c\n", moves[4], moves[5], moves[6]);
  printf("----------------|---------------|--------------\n");
  printf("\t%c\t|\t%c\t|\t%c\n\n", moves[7], moves[8], moves[9]);
}

void markBoard() {
  drawBoard(moves);
  do {
    printf("Player 1, where do you want to place your X?\n");
    scanf("%i", &p1Move);
  } while (!pMove(p1Move));
  moves[p1Move] = 'X';
  drawBoard(moves);
  if (!haveWinner()) {
    do {
      printf("Player 2, where do you want to place your O?\n");
      scanf("%i", &p2Move);
    } while (!pMove(p2Move));
    moves[p2Move] = 'O';
    drawBoard(moves);
  }
}

bool pMove(int selection) {
  return moves[selection] == 'X' ? false : moves[selection] == 'O' ? false : (selection < 1 || selection > 9) ? false : true;
}
